import UIKit

var myName = "Andrey Gamaliy"
print(myName)

// Домашнее задание №2

//Задача 1
let milkmanPhrase = "Молоко - это  полезно"
print(milkmanPhrase)

// Задача 2
var milkPrice: Double = 3.0

//Задача 3
milkPrice = 4.20

//Задача 4
var milkBottleCount: Int? = 20
var profit: Double = 0.0

profit = Double(milkPrice) * Double(milkBottleCount!)

print(profit)

// Дополнительное задание*
// force unwrap не безопасен, потому что, если переменная milkBottleCount будет иметь значение nil, то программа завершится ошибкой, т.е. мы на 100% должны быть уверены в наличии значения опционала при использовании force unwrap.
if let bottle = milkBottleCount {
    profit = milkPrice * Double(bottle)
    print(profit)
} else {
    print("milkBottleCount == nil")
}

// другой пример опционального связывания
let firstName: String? = nil
let secondName = "Gamaliy"
var fullName = ""

if let name = firstName {
    fullName = name + " " + secondName
    print(fullName)
} else {
    print("Имя отcутствует")
}


//Задача 5
var employeesList: [String] = []
employeesList = Array(arrayLiteral: "Петр", "Геннадий", "Иван", "Андрей", "Марфа")

//Задача 6
var isEveryoneWorkHard: Bool = false
var workingHours = 39

if workingHours >= 40 {
    isEveryoneWorkHard = true
} else if workingHours < 40 {
    isEveryoneWorkHard = false
}

print(isEveryoneWorkHard)

// Домашнее задание №3, Задача 1

func makeBuffer() -> (String) -> Void {
    var message: String = ""
    var closure = { (text: String) -> Void in
        
        if text.isEmpty {
            print(message)
        } else {
            message += text
        }
    }
    return closure
}

var buffer = makeBuffer()

buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("") // Замыкание использовать нужно!

// Домашнее задание №3, 2 задание
func checkPrimeNumber(_ number: Int) -> Bool {
    guard number >= 2 else { return false }
    
    for i in 2 ..< number {
        if number % i == 0 {
            return false
        }
    }
    return true
}

checkPrimeNumber(7) // true
checkPrimeNumber(8) // false
checkPrimeNumber(13) // true

// Домашнее задание №4, 1 задание
struct Device {
    let name: String
    let screenSize: CGSize
    let screenDiagonal: Double
    let scaleFactor: Int

    static let iPhone14Pro = Device(name: "iPhone 14 Pro", screenSize: CGSize(width: 393, height: 852), screenDiagonal: 6.1, scaleFactor: 3)
    static let iPhoneXR = Device(name: "iPhone XR", screenSize: CGSize(width: 414, height: 896), screenDiagonal: 6.06, scaleFactor: 2)
    static let iPadPro = Device(name: "iPad Pro", screenSize: CGSize(width: 834, height: 1194), screenDiagonal: 11, scaleFactor: 2)

    func physicalSize() -> CGSize {
        return CGSize(
            width: screenSize.width * CGFloat(scaleFactor),
            height: screenSize.height * CGFloat(scaleFactor)
        )
    }
}
